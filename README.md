# Orders play - REST API demo

## web service
### products CRUD operations
 - crate a new product
 - get list of all products
 - update a product
 - get product by id
 
### orders endpoint
 - place an order (has list of products)
 - retrieve all orders within a given time period

## Implementation details
 - Spring Boot 2
 - Lombok
 - springdoc-openapi-ui
 - MariaDB in docker

## Starting MariaDb
1. build docker image
```
  $ cd db  
  $ docker build -t orders-1 .
```  
2. run docker image
```
  $ docker run --name orders-mariadb -p 3306:3306 -d orders-1:latest
```  

## Documentation using Open API
- http://localhost:9292/rest-play/v3/api-docs/  
- http://localhost:9292/rest-play/swagger-ui.html 

### Reference
- https://github.com/springdoc/springdoc-openapi
- https://springdoc.github.io/springdoc-openapi-demos/

## Code coverage using jacoco
```
  $ mvn clean test
```  
Open in browser:    
file:///PROJECT_DIR/target/site/jacoco//index.html  

- https://mkyong.com/maven/maven-jacoco-code-coverage-example/  

## Security
 - [REST API Security Essentials](https://restfulapi.net/security-essentials/)
 - [Spring Security Reference](https://docs.spring.io/spring-security/site/docs/current/reference/html5/)
 
### TODO
 - HTTPS
 - basic auth
 - OAuth2
 
## Reference Documentation
For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.2.5.RELEASE/maven-plugin/)
* [Spring Web](https://docs.spring.io/spring-boot/docs/2.2.5.RELEASE/reference/htmlsingle/#boot-features-developing-web-applications)
* [Spring Data JPA](https://docs.spring.io/spring-boot/docs/2.2.5.RELEASE/reference/htmlsingle/#boot-features-jpa-and-spring-data)

## Guides
The following guides illustrate how to use some features concretely:

* [Building a RESTful Web Service](https://spring.io/guides/gs/rest-service/)
* [Serving Web Content with Spring MVC](https://spring.io/guides/gs/serving-web-content/)
* [Building REST services with Spring](https://spring.io/guides/tutorials/bookmarks/)
* [Accessing Data with JPA](https://spring.io/guides/gs/accessing-data-jpa/)

