https://hub.docker.com/_/mariadb

## Start
$ docker run --name orders-mariadb -p -e MYSQL_ROOT_PASSWORD=pwd -e MYSQL_DATABASE=orders -d mariadb:10.5.1
$ docker run --name orders-mariadb -p 3306:3306 -e MYSQL_ROOT_PASSWORD=pwd -e MYSQL_DATABASE=orders -d mariadb:10.5.1

### Create docker image to create tables
$ docker build -t orders-1 .
$ docker run --name orders-mariadb -p 3306:3306 -d orders-1:latest

## JDBC URL
- jdbc:mariadb://localhost:3306/orders
- jdbc:mariadb://192.168.99.100:3306/orders

## Logs
$ docker logs orders-mariadb

## Bash
$ docker exec -it orders-mariadb bash
Via git bash:    
$ winpty docker exec -it orders-mariadb bash

### Connecting to MariaDB
mysql -u root -ppwd -h localhost

CREATE DATABASE orders;
USE orders;

## Inspect
$ docker inspect orders-mariadb

## remove
docker rm orders-mariadb

## Docker compose
- schema.sql is copied as directory into container?!
