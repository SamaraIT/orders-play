package hr.samarait.demo.common.utils;

import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.http.MediaType;

import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FileUtilsTest {

  @ParameterizedTest
  @MethodSource
  void resolveMediaType(String fileName, MediaType expectedMediaType) {
    MediaType mediaType = FileUtils.resolveMediaType(fileName);
    assertEquals(expectedMediaType, mediaType);
  }

  private static Stream<Arguments> resolveMediaType() {
    return Stream.of(
      Arguments.of("image.jpeg", MediaType.IMAGE_JPEG),
      Arguments.of("image.jpg", MediaType.IMAGE_JPEG),
      Arguments.of("image.JPG", MediaType.IMAGE_JPEG),
      Arguments.of("image.jPeG", MediaType.IMAGE_JPEG),
      Arguments.of("image.gif", MediaType.IMAGE_GIF),
      Arguments.of("image.png", MediaType.IMAGE_PNG),
      Arguments.of(".png", MediaType.IMAGE_PNG),
      Arguments.of("images/he-man_400.jpg", MediaType.IMAGE_JPEG),
      Arguments.of("image.tif", new MediaType("image", "tiff")),
      Arguments.of("image.PDF", new MediaType("application", "pdf")),
      Arguments.of("image", MediaType.APPLICATION_OCTET_STREAM),
      Arguments.of("", MediaType.APPLICATION_OCTET_STREAM),
      Arguments.of(null, MediaType.APPLICATION_OCTET_STREAM)
    );
  }

}
