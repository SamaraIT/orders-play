package hr.samarait.demo.products.web;

import com.fasterxml.jackson.databind.ObjectMapper;
import hr.samarait.demo.common.entity.Amount;
import hr.samarait.demo.common.exception.ProductNotFoundException;
import hr.samarait.demo.products.entity.Product;
import hr.samarait.demo.products.repository.ProductRepository;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.math.BigDecimal;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class ProductIntegrationTest {

  @Autowired
  private MockMvc mockMvc;
  @Autowired
  private ObjectMapper objectMapper;
  @Autowired
  private ProductRepository repository;

  private static final String PRODUCTS_URL = "/products";
  private static final int PAGE_SIZE = 64;

  private static String productId;

  @Test
  @Order(1)  void setup() {
    repository.deleteAll();
  }

  @Test
  @Order(5)
  void fetchProducts_empty() throws Exception {
    mockMvc.perform(get(PRODUCTS_URL))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.content").exists())
      .andExpect(jsonPath("$.content[*]").isEmpty())
      .andExpect(jsonPath("$.pageable").exists())
      .andExpect(jsonPath("$.pageable.pageNumber").value(0))
      .andExpect(jsonPath("$.size").value(PAGE_SIZE))
      .andExpect(jsonPath("$.totalElements").value(0))
      .andExpect(jsonPath("$.totalPages").value(0))
      .andExpect(jsonPath("$.sort.sorted").value(true));
  }

  @Test
  @Order(6)
  void fetchProduct_wrongId_error() throws Exception {
    String paymentId = "wrongId";
    mockMvc.perform(get(PRODUCTS_URL + "/" + paymentId))
      .andDo(print())
      .andExpect(status().is(404))
      .andExpect(jsonPath("$.errors[0]").value(ProductNotFoundException.MSG + paymentId));
  }

  @Test
  @Order(7)
  void createProduct_badRequest() throws Exception {
    final Product request = new Product();
    mockMvc.perform(post(PRODUCTS_URL)
      .contentType(MediaType.APPLICATION_JSON)
      .content(objectMapper.writeValueAsString(request)))
      .andDo(print())
      .andExpect(status().isBadRequest())
      .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
      .andExpect(jsonPath("$.errors[*]", Matchers.containsInAnyOrder("name: must not be blank", "price: must not be null")));
  }

  @Test
  @Order(10)
  void createProduct_ok() throws Exception {
    final Product request = new Product();
    request.setName("my first product");
    request.setPrice(new Amount("EUR", new BigDecimal("12.34")));

    MvcResult result = mockMvc.perform(post(PRODUCTS_URL)
      .contentType(MediaType.APPLICATION_JSON)
      .content(objectMapper.writeValueAsString(request)))
      .andDo(print())
      .andExpect(status().isCreated())
      .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
      .andExpect(jsonPath("$.id").isNotEmpty())
      .andExpect(jsonPath("$.description").isEmpty())
      .andExpect(jsonPath("$.modified").isEmpty())
      .andExpect(jsonPath("$.name").value(request.getName()))
      .andExpect(jsonPath("$.price.currency").value(request.getPrice().getCurrency()))
      .andExpect(jsonPath("$.price.amount").value(request.getPrice().getAmount().toString()))
      .andReturn();

    productId = objectMapper.readValue(result.getResponse().getContentAsString(), Product.class).getId();
    System.out.println("\n *** createProduct TEST, productId=" + productId);
    Assertions.assertNotNull(productId);
  }

  @Test
  @Order(15)
  void updateProduct_ok() throws Exception {
    final Product request = new Product();
    request.setName("updated product");
    request.setPrice(new Amount("USD", new BigDecimal("56.78")));

    MvcResult result = mockMvc.perform(put(PRODUCTS_URL + "/" + productId)
      .contentType(MediaType.APPLICATION_JSON)
      .content(objectMapper.writeValueAsString(request)))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
      .andExpect(jsonPath("$.id").value(productId))
      .andExpect(jsonPath("$.created").isNotEmpty())
      .andExpect(jsonPath("$.modified").isNotEmpty())
      .andExpect(jsonPath("$.name").value(request.getName()))
      .andExpect(jsonPath("$.price.currency").value(request.getPrice().getCurrency()))
      .andExpect(jsonPath("$.price.amount").value(request.getPrice().getAmount().toString()))
      .andExpect(jsonPath("$.description").isEmpty())
      .andReturn();

    productId = objectMapper.readValue(result.getResponse().getContentAsString(), Product.class).getId();
    Assertions.assertNotNull(productId);
  }

  @Test
  @Order(16)
  void updateProduct_error() throws Exception {
    final Product request = new Product();
    request.setName("updated product");
    request.setPrice(new Amount("too many", new BigDecimal("56.78")));
    request.setDescription("wrong currency");

    mockMvc.perform(put(PRODUCTS_URL + "/" + productId)
      .contentType(MediaType.APPLICATION_JSON)
      .content(objectMapper.writeValueAsString(request)))
      .andDo(print())
      .andExpect(status().isBadRequest())
      .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
      .andExpect(jsonPath("$.errors[0]").value("currency: size must be between 3 and 3"));
  }

  @Test
  @Order(20)
  void fetchProducts() throws Exception {
    mockMvc.perform(get(PRODUCTS_URL))
      .andDo(print())
      // {"content":[{"id":"0e135ba5-3c39-42e3-ad30-112f0af0195a","name":"updated product","description":null,"price":{"currency":"USD","amount":56.78},"created":"2020-03-14T13:17:41.462242","modified":"2020-03-14T13:17:41.462242"}],"pageable":{"sort":{"sorted":true,"unsorted":false,"empty":false},"offset":0,"pageNumber":0,"pageSize":64,"unpaged":false,"paged":true},"totalPages":1,"totalElements":1,"last":true,"size":64,"number":0,"sort":{"sorted":true,"unsorted":false,"empty":false},"numberOfElements":1,"first":true,"empty":false}
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.content").isNotEmpty())
      .andExpect(jsonPath("$.content[0].id").value(productId))
      .andExpect(jsonPath("$.content[0].name").value("updated product"))
      .andExpect(jsonPath("$.content[0].price.currency").value("USD"))
      .andExpect(jsonPath("$.content[0].price.amount").value("56.78"))
      .andExpect(jsonPath("$.content[0].created").isNotEmpty())
      .andExpect(jsonPath("$.content[0].modified").exists())
      .andExpect(jsonPath("$.pageable").exists())
      .andExpect(jsonPath("$.pageable.pageNumber").value(0))
      .andExpect(jsonPath("$.size").value(PAGE_SIZE))
      .andExpect(jsonPath("$.totalElements").value(1))
      .andExpect(jsonPath("$.totalPages").value(1))
      .andExpect(jsonPath("$.sort.sorted").value(true));
  }


}
