package hr.samarait.demo.orders.web;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.spi.json.JacksonJsonProvider;
import com.jayway.jsonpath.spi.json.JsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.jayway.jsonpath.spi.mapper.MappingProvider;
import hr.samarait.demo.common.entity.Amount;
import hr.samarait.demo.orders.repository.OrderRepository;
import hr.samarait.demo.orders.web.model.OrderRequest;
import hr.samarait.demo.products.entity.Product;
import hr.samarait.demo.products.repository.ProductRepository;
import hr.samarait.demo.products.service.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.hamcrest.Matchers;
import org.junit.jupiter.api.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.collection.IsIterableContainingInOrder.contains;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.MOCK)
@AutoConfigureMockMvc
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
@Slf4j
class OrderIntegrationTest {

  private static final String ORDERS_URL = "/orders";
  private static final int PAGE_SIZE = 64;
  private static final String DATE_FROM = LocalDateTime.now().minusDays(10).format(DateTimeFormatter.ISO_DATE_TIME);
  private static final String DATE_TO = LocalDateTime.now().plusMinutes(5).format(DateTimeFormatter.ISO_DATE_TIME);

  @Autowired
  private MockMvc mockMvc;
  private ObjectMapper objectMapper;
  @Autowired
  private ProductService productService;
  @Autowired
  private ProductRepository productRepository;
  @Autowired
  private OrderRepository orderRepository;

  // https://dev.to/piczmar_0/json-conversion-errors-with-spring-mockmvc-2ma1
  @BeforeEach
  void configureObjectMapper() {
    objectMapper = new ObjectMapper();
    objectMapper.enable(DeserializationFeature.USE_LONG_FOR_INTS);
    objectMapper.enable(DeserializationFeature.USE_BIG_DECIMAL_FOR_FLOATS); // default is Double

    configJsonProvider(objectMapper);
  }

  @Test
  @Order(3)
  void setup_add_2_products() {
    productRepository.deleteAll();
    orderRepository.deleteAll();

    Product product = new Product();
    product.setName("my first product");
    product.setPrice(new Amount("EUR", new BigDecimal("12.34")));
    productService.save(product);

    product.setName("second product");
    product.getPrice().setAmount(new BigDecimal("56.78"));
    productService.save(product);
  }

  @Test
  @Order(10)
  void createOrder_badRequest() throws Exception {
    OrderRequest request = new OrderRequest();
    log.debug("Prepared request={}", request);

    mockMvc.perform(post(ORDERS_URL)
      .contentType(MediaType.APPLICATION_JSON)
      .content(objectMapper.writeValueAsString(request)))
      .andDo(print())
      .andExpect(status().isBadRequest())
      .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
      .andExpect(jsonPath("$.errors[*]", Matchers.containsInAnyOrder("email: must not be blank",
        "items: must not be empty", "currency: must not be null")));
  }

  @Test
  @Order(20)
  void createOrder() throws Exception {
    Page<Product> products = productService.findAll(PageRequest.of(0, 2));
    log.debug("Fetched products={}", products.getContent());

    List<OrderRequest.ItemDto> items = new ArrayList<>();
    BigDecimal amount = BigDecimal.ZERO;
    List<String> names = new ArrayList<>();
    List<BigDecimal> amounts = new ArrayList<>();
    for (var product : products) {
      OrderRequest.ItemDto itemDto = new OrderRequest.ItemDto();
      itemDto.setProductId(product.getId());
      itemDto.setQuantity(1);
      BigDecimal itemAmount = product.getPrice().getAmount().multiply(BigDecimal.valueOf(itemDto.getQuantity()));
      amount = amount.add(itemAmount);
      names.add(product.getName());
      amounts.add(itemAmount);
      items.add(itemDto);
    }
    OrderRequest request = new OrderRequest();
    request.setCurrency("EUR");
    request.setEmail("igor@samarait.hr");
    request.setItems(items);
    log.debug("Prepared request={}", request);

    mockMvc.perform(post(ORDERS_URL)
      .contentType(MediaType.APPLICATION_JSON)
      .content(objectMapper.writeValueAsString(request)))
      .andDo(print())
      .andExpect(status().isCreated())
      .andExpect(header().string(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_VALUE))
      .andExpect(jsonPath("$.id").isNotEmpty())
      .andExpect(jsonPath("$.email").value(request.getEmail()))
      .andExpect(jsonPath("$.created").isNotEmpty())
      .andExpect(jsonPath("$.price.currency").value("EUR"))
      .andExpect(jsonPath("$.price.amount").value(amount.toString()))
      .andExpect(jsonPath("$.items[*].name", contains(names.toArray())))
      .andExpect(jsonPath("$.items[*].amount", contains(amounts.toArray())));
  }

  @Test
  @Order(30)
  void fetchOrders() throws Exception {
    List<String> names = productService.findAll(PageRequest.of(0, 2))
      .stream()
      .map(Product::getName)
      .collect(Collectors.toList());

    mockMvc.perform(get(ORDERS_URL)
      .param("startDate", DATE_FROM)
      .param("endDate", DATE_TO))
      .andDo(print())
      .andExpect(status().isOk())
      .andExpect(jsonPath("$.content").exists())
      .andExpect(jsonPath("$.pageable").exists())
      .andExpect(jsonPath("$.pageable.pageNumber").value(0))
      .andExpect(jsonPath("$.size").value(PAGE_SIZE))
      .andExpect(jsonPath("$.totalElements").value(1))
      .andExpect(jsonPath("$.totalPages").value(1))
      .andExpect(jsonPath("$.sort.sorted").value(true))
      .andExpect(jsonPath("$.content[0].id").isNotEmpty())
      .andExpect(jsonPath("$.content[0].email").value("igor@samarait.hr"))
      .andExpect(jsonPath("$.content[0].created").isNotEmpty())
      .andExpect(jsonPath("$.content[0].price.currency").value("EUR"))
      .andExpect(jsonPath("$.content[0].price.amount").value( new BigDecimal("69.12")))
      .andExpect(jsonPath("$.content[0].items[*].name", contains(names.toArray())));
  }

  @Test
  @Order(40)
  void fetchOrders_endDate_missing_error() throws Exception {
    mockMvc.perform(get(ORDERS_URL)
      .param("startDate", DATE_FROM))
      .andDo(print())
      .andExpect(status().isBadRequest())
      .andExpect(jsonPath("$.errors[0]").value("endDate parameter is missing"));
  }

  @Test
  @Order(41)
  void fetchOrders_date_format_error() throws Exception {
    mockMvc.perform(get(ORDERS_URL)
      .param("startDate", "2020-01-01-time is missing"))
      .andDo(print())
      .andExpect(status().isBadRequest())
      .andExpect(jsonPath("$.errors[0]").value("startDate is not in proper format"));
  }

  private void configJsonProvider(ObjectMapper objectMapper) {

    Configuration.setDefaults(new Configuration.Defaults() {

      private final JsonProvider jsonProvider = new JacksonJsonProvider(objectMapper);
      private final MappingProvider mappingProvider = new JacksonMappingProvider(objectMapper);

      @Override
      public JsonProvider jsonProvider() {
        return jsonProvider;
      }

      @Override
      public MappingProvider mappingProvider() {
        return mappingProvider;
      }

      @Override
      public Set<Option> options() {
        return EnumSet.noneOf(Option.class);
      }
    });
  }
}
