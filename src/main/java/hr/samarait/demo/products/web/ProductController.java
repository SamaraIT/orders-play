package hr.samarait.demo.products.web;

import hr.samarait.demo.products.entity.Product;
import hr.samarait.demo.products.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Products endpoint
 */
@RestController()
@RequestMapping("/products")
@Tag(name = "products")
@Slf4j
public class ProductController {

  private final ProductService productService;

  public ProductController(ProductService productService) {
    this.productService = productService;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @Operation(summary = "Create a product")
  public Product createProduct(@RequestBody @Valid Product product) {
    log.info("createProduct START {}", product);
    var savedProduct = productService.save(product);
    log.info("createProduct END {}", savedProduct);
    return savedProduct;
  }

  @PutMapping("/{id}")
  @Operation(summary = "Update a product",
    description = "Updates the specific product by setting the values of the parameters passed. Any parameters not provided will be left unchanged.")
  public Product updateProduct(@PathVariable String id, @RequestBody Product product) {
    log.info("updateProduct START id={}, product={}", id, product);
    Product updatedProduct = productService.updateProduct(id, product);
    log.info("updateProduct END {}", updatedProduct);
    return updatedProduct;
  }

  @GetMapping
  @Operation(summary = "Retrieve products")
  public Page<Product> fetchProducts(@SortDefault(sort = "created", direction = Sort.Direction.DESC) Pageable pageable) {
    log.info("fetchProducts START {}", pageable);
    Page<Product> products = productService.findAll(pageable);
    log.info("fetchProducts END {}", products);
    return products;
  }

  @GetMapping("/{id}")
  @Operation(summary = "Retrieve a product by id")
  public Product fetchProduct(@PathVariable String id) {
    log.info("fetchProduct START id={}", id);
    Product product = productService.findById(id);
    log.info("fetchProduct END {}", product);
    return product;
  }
}
