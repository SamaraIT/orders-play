package hr.samarait.demo.products.entity;

import hr.samarait.demo.common.entity.Amount;
import lombok.Data;

import javax.persistence.*;
import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;

@Data
@Entity
@Table(name = "PRODUCTS")
public class Product {

  @Id
  private String id;

  @NotBlank
  @Column(unique=true)
  private String name;

  private String description;

  @Embedded
  @NotNull
  @Valid
  private Amount price;

  private LocalDateTime created = LocalDateTime.now();
  private LocalDateTime modified;
}
