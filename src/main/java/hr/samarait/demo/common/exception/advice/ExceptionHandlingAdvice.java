package hr.samarait.demo.common.exception.advice;

import hr.samarait.demo.common.exception.ErrorResponse;
import hr.samarait.demo.common.exception.ItemNotFoundException;
import hr.samarait.demo.common.exception.ProductNotFoundException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
@Slf4j
public class ExceptionHandlingAdvice {

  @ExceptionHandler(ProductNotFoundException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.NOT_FOUND)
  public ErrorResponse handleProductNotFound(ProductNotFoundException ex) {
    return new ErrorResponse(ex.getMessage());
  }

  @ExceptionHandler(ItemNotFoundException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ErrorResponse handleItemNotFound(ItemNotFoundException ex) {
    return new ErrorResponse(ex.getMessage());
  }

  @ExceptionHandler({IllegalArgumentException.class, IllegalStateException.class})
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ErrorResponse illegalExceptionHandler(RuntimeException ex) {
    log.info("IllegalException occurred", ex);
    return new ErrorResponse(ex.getMessage());
  }

  /**
   * argument annotated with @Valid fails validation
   */
  @ExceptionHandler({MethodArgumentNotValidException.class})
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ErrorResponse handleValidationException(MethodArgumentNotValidException ex) {
    log.info("MethodArgumentNotValidException occurred", ex.getMessage());
    List<String> errors = new ArrayList<>();
    for (FieldError error : ex.getBindingResult().getFieldErrors()) {
      errors.add(error.getField() + ": " + error.getDefaultMessage());
    }
    for (ObjectError error : ex.getBindingResult().getGlobalErrors()) {
      errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
    }
    return new ErrorResponse(errors);
  }

  @ExceptionHandler({ConstraintViolationException.class})
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ErrorResponse handleConstraintViolation(ConstraintViolationException ex) {
    List<String> errors = new ArrayList<>();
    for (ConstraintViolation<?> violation : ex.getConstraintViolations()) {
      log.info("ConstraintViolationException occurred: {}", violation.getRootBeanClass().getName() + " " +
        violation.getPropertyPath() + ": " + violation.getMessage());
      String msg = violation.getMessage();
      if (violation.getPropertyPath() != null) {
        msg = violation.getPropertyPath() + ": " + violation.getMessage();
        errors.add(msg);
      }
    }
    return new ErrorResponse(errors);
  }

  /**
   * mandatory request params are not present
   */
  @ExceptionHandler({MissingServletRequestParameterException.class})
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  protected ErrorResponse handleMissingServletRequestParameter(MissingServletRequestParameterException ex) {
    log.error(ex.getMessage());
    String msg = ex.getParameterName() + " parameter is missing";
    return new ErrorResponse(msg);
  }

  @ExceptionHandler(MethodArgumentTypeMismatchException.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.BAD_REQUEST)
  public ErrorResponse handleTypeMismatch(MethodArgumentTypeMismatchException ex) {
    log.error(ex.getMessage());
    String message = ex.getName() + " is not in proper format";
    return new ErrorResponse(message);
  }

  @ExceptionHandler(Exception.class)
  @ResponseBody
  @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
  public ErrorResponse handleTypeMismatch(Exception ex) {
    log.error("Unhandled exception occurred " + ex.getClass().getSimpleName() + ", message: " + ex.getMessage(), ex);
    String msg = "An unexpected error occurred at " + LocalDateTime.now() + ". Please contact provider with timestamp";
    return new ErrorResponse(msg);
  }


}


