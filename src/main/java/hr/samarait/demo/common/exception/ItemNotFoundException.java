package hr.samarait.demo.common.exception;

public class ItemNotFoundException extends RuntimeException {

  public static final String MSG = "Could not find item with productId=";

  public ItemNotFoundException(String id) {
    super(MSG + id);
  }
}
