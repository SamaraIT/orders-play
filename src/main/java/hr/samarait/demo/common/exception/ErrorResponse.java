package hr.samarait.demo.common.exception;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Data
@AllArgsConstructor
public class ErrorResponse implements Serializable {
  private final List<String> errors;

  public ErrorResponse(String text) {
    errors = new ArrayList<>();
    errors.add(text);
  }


}
