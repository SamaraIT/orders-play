package hr.samarait.demo.common.exception;

public class ProductNotFoundException extends RuntimeException {

  public static final String MSG = "Could not find product with id=";

  public ProductNotFoundException(String id) {
    super(MSG + id);
  }
}
