package hr.samarait.demo.common.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.math.BigDecimal;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Embeddable
public class Amount implements Serializable {

  /**
   * Currency Code
   */
  @NotNull
  @Size(min = 3, max = 3)
  private String currency;

  /**
   * The amount given with fractional digits, where fractions must be compliant to the currency definition.<br>
   * The decimal separator is a dot.
   *
   * <ul>Example: Valid representations for EUR with up to two decimals are:
   * <li>1056</li>
   * <li>5768.2</li>
   * <li>-1.50</li>
   * <li>5877.78</li>
   * </ul>
   */
  @NotNull
  private BigDecimal amount;

}
