package hr.samarait.demo.common.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;

import java.net.URLConnection;

@Slf4j
public class FileUtils {

  public static MediaType resolveMediaType(String fileName) {
    MediaType mediaType;
    try {
      String mimeType = URLConnection.guessContentTypeFromName(fileName);
      log.trace("URLConnection.guessContentTypeFromName {}", mimeType);
      mediaType = MediaType.parseMediaType(mimeType);
    } catch (Exception e) {
      log.warn("Could not resolveMediaType from {} - using default application/octet-stream", fileName);
      mediaType = MediaType.APPLICATION_OCTET_STREAM;
    }
    log.debug("resolved mediaType to {}", mediaType);
    return mediaType;
  }
}
