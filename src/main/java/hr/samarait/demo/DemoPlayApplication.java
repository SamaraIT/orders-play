package hr.samarait.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DemoPlayApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoPlayApplication.class, args);
	}

}
