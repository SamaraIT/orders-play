package hr.samarait.demo.orders.service;

import hr.samarait.demo.common.entity.Amount;
import hr.samarait.demo.common.exception.ItemNotFoundException;
import hr.samarait.demo.orders.entity.Item;
import hr.samarait.demo.orders.entity.Order;
import hr.samarait.demo.orders.repository.OrderRepository;
import hr.samarait.demo.orders.web.model.OrderRequest;
import hr.samarait.demo.orders.web.model.OrderResponse;
import hr.samarait.demo.products.entity.Product;
import hr.samarait.demo.products.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

@Service
@Slf4j
public class OrderService {

  private final OrderRepository orderRepository;
  private final ProductRepository productRepository;

  public OrderService(OrderRepository orderRepository, ProductRepository productRepository) {
    this.orderRepository = orderRepository;
    this.productRepository = productRepository;
  }

  public Page<OrderResponse> findByCreatedBetween(LocalDateTime start, LocalDateTime end, Pageable pageable) {
    Page<Order> orders = orderRepository.findByCreatedBetween(start, end, pageable);
    log.debug("findByCreatedBetween {}", orders.getPageable());
    return orders.map(OrderService::toOrderResponse);
  }

  public OrderResponse createOrder(OrderRequest request) {
    Order newOrder = new Order();
    newOrder.setId(UUID.randomUUID().toString());
    newOrder.setEmail(request.getEmail());
    newOrder.setCurrency(request.getCurrency());
    for (OrderRequest.ItemDto itemDto : request.getItems()) {
      Product product = productRepository.findById(itemDto.getProductId())
        .orElseThrow(() -> new ItemNotFoundException(itemDto.getProductId()));


      Item item = new Item();
      if (Objects.equals(product.getPrice().getCurrency(), request.getCurrency())) {
        item.setAmount(product.getPrice().getAmount());
      } else {
        // TODO currency conversion
        item.setAmount(product.getPrice().getAmount().multiply(BigDecimal.TEN));
      }
      item.setId(UUID.randomUUID().toString());
      item.setName(product.getName());
      item.setQuantity(itemDto.getQuantity());
      newOrder.getItems().add(item);
    }
    log.debug("created newOrder={}", newOrder);
    Order savedOrder = orderRepository.save(newOrder);
    log.debug("saved order={}", savedOrder);

    OrderResponse orderResponse = new OrderResponse();
    orderResponse.setId(savedOrder.getId());
    orderResponse.setCreated(savedOrder.getCreated());
    orderResponse.setEmail(savedOrder.getEmail());
    BigDecimal amount = OrderService.convertItems(savedOrder, orderResponse);
    orderResponse.setPrice(new Amount(savedOrder.getCurrency(), amount));
    return orderResponse;
  }

  private static BigDecimal convertItems(Order order, OrderResponse orderResponse) {
    BigDecimal amount = BigDecimal.ZERO;
    for (Item item : order.getItems()) {
      OrderResponse.ItemDto itemRes = new OrderResponse.ItemDto();
      itemRes.setName(item.getName());
      itemRes.setAmount(item.getAmount());
      orderResponse.getItems().add(itemRes);
      amount = amount.add(item.getAmount().multiply(BigDecimal.valueOf(item.getQuantity())));
    }
    return amount;
  }


  private static OrderResponse toOrderResponse(Order order) {
    OrderResponse response = new OrderResponse();
    response.setId(order.getId());
    response.setEmail(order.getEmail());
    response.setCreated(order.getCreated());
    BigDecimal amount = OrderService.convertItems(order, response);
    response.setPrice(new Amount(order.getCurrency(), amount));
    return response;
  }

}
