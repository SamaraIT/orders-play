package hr.samarait.demo.orders.web;

import hr.samarait.demo.orders.service.OrderService;
import hr.samarait.demo.orders.web.model.OrderRequest;
import hr.samarait.demo.orders.web.model.OrderResponse;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDateTime;

/**
 * Orders endpoint
 */
@RestController()
@RequestMapping("/orders")
@Tag(name = "orders")
@Slf4j
public class OrderController {

  private final OrderService orderService;

  public OrderController(OrderService orderService) {
    this.orderService = orderService;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  @Operation(summary = "Place an order")
  public OrderResponse createOrder(@RequestBody @Valid OrderRequest request) {
    log.info("createOrder START {}", request);
    OrderResponse order = orderService.createOrder(request);
    log.info("createOrder END {}", order);
    return order;
  }

  @GetMapping
  @Operation(summary = "Retrieve orders within a given time period")
  public Page<OrderResponse> fetchOrders(@RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime startDate,
                                         @RequestParam @DateTimeFormat(iso = DateTimeFormat.ISO.DATE_TIME) LocalDateTime endDate,
                                         @SortDefault(sort = "created", direction = Sort.Direction.DESC) Pageable pageable) {
    log.info("fetchOrders START startDate={}, endDate={}, pageable={}", startDate, endDate, pageable);
    Page<OrderResponse> orders = orderService.findByCreatedBetween(startDate, endDate, pageable);
    log.info("fetchOrders END {}", orders);
    return orders;
  }

}
