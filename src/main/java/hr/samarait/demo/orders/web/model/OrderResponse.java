package hr.samarait.demo.orders.web.model;

import hr.samarait.demo.common.entity.Amount;
import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Data
public class OrderResponse {

  private String id;
  private LocalDateTime created;
  private String email;

  private List<ItemDto> items = new ArrayList<>();

  private Amount price;

  @Data
  public static class ItemDto {
    private String name;
    private BigDecimal amount;
  }

}


