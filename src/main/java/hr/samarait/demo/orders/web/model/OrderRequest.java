package hr.samarait.demo.orders.web.model;

import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.*;
import java.util.List;

@Data
public class OrderRequest {

  @NotBlank
  @Email
  private String email;

  @Valid
  @NotEmpty
  private List<ItemDto> items;

  /**
   * Currency Code
   */
  @NotNull
  @Size(min = 3, max = 3)
  private String currency;

  @Data
  public static class ItemDto {
    @NotBlank
    private String productId;
    @Positive
    private int quantity;
  }

}


