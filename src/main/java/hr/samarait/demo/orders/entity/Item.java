package hr.samarait.demo.orders.entity;

import lombok.Data;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;

/**
 * subset of product information - taken from Product when order takes place
 */
@Data
@Entity
@Table(name = "ITEMS")
public class Item {

  @Id
  private String id;

  @NotBlank
  @Column(unique = true)
  private String name;

  private int quantity;

  @NotNull
  private BigDecimal amount;

}
