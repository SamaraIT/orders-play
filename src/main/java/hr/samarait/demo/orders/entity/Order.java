package hr.samarait.demo.orders.entity;

import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * subset of product information - taken from Product when order takes place
 */
@Data
@Entity
@Table(name = "ORDERS")
public class Order {

  @Id
  private String id;

  @NotBlank
  @Email
  private String email;

  private LocalDateTime created = LocalDateTime.now();

  @NotNull
  @Size(min = 3, max = 3)
  private String currency;

  @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true  )
  @JoinColumn(name = "order_id")
  private List<Item> items = new ArrayList<>();

}
