package hr.samarait.demo.tx.show.web;

import hr.samarait.demo.common.entity.Amount;
import hr.samarait.demo.common.exception.ProductNotFoundException;
import hr.samarait.demo.products.entity.Product;
import hr.samarait.demo.products.repository.ProductRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.time.LocalDateTime;
import java.util.Set;
import java.util.UUID;

@Service
@Slf4j
public class TxService {

  private final ProductRepository productRepository;

  public TxService(ProductRepository productRepository) {
    this.productRepository = productRepository;
  }

  public Product save(Product product) {
    product.setId(UUID.randomUUID().toString());
    return productRepository.save(product);
  }

  public Product save_throw_RuntimeException_NO_annotation(Product product) {
    product.setId(UUID.randomUUID().toString());
    log.info("Saving product with i={}", product.getId());
    productRepository.save(product);
    throw new RuntimeException("RuntimeException - something went wrong - plx rollback");
  }

  @Transactional(noRollbackFor = RuntimeException.class)
  //@Transactional
  public Product save_throw_RuntimeException_WITH_annotation(Product product) {
    product.setId(UUID.randomUUID().toString());
    log.info("Saving product with i={}", product.getId());
    productRepository.save(product);
    throw new RuntimeException("RuntimeException - something went wrong - plx rollback");
  }

  //@Transactional(noRollbackFor = Exception.class)
  @Transactional
  public Product save_throw_Exception_WITH_annotation(Product product) throws Exception {
    product.setId(UUID.randomUUID().toString());
    log.info("Saving product with i={}", product.getId());
    productRepository.save(product);
    throw new Exception("Exception - something went wrong - plx rollback");
  }

  public Product saveWithoutAnnotation_callingAnnotatedMethod(Product product) {
    log.info("Calling transactional method desc={}", product.getDescription());
    return save_throw_RuntimeException_WITH_annotation(product);
  }

  public Product findById(String id) {
    return productRepository.findById(id)
      .orElseThrow(() -> new ProductNotFoundException(id));
  }

  public Page<Product> findAll(Pageable pageable) {
    return productRepository.findAll(pageable);
  }

  public Product updateProduct(String id, Product product) {
    Product fetchedProduct = productRepository.findById(id)
      .orElseThrow(() -> new ProductNotFoundException(id));
    log.debug("fetched {}", fetchedProduct);

    updateChangedFields(product, fetchedProduct);
    return productRepository.save(fetchedProduct);
  }

  private void updateChangedFields(Product product, Product fetchedProduct) {
    if (StringUtils.isNotBlank(product.getName())) {
      fetchedProduct.setName(product.getName());
    }
    if (StringUtils.isNotBlank(product.getDescription())) {
      fetchedProduct.setDescription(product.getDescription());
    }
    if (product.getPrice() != null) {
      if (product.getPrice().getAmount() != null) {
        fetchedProduct.getPrice().setAmount(product.getPrice().getAmount());
      }
      if (StringUtils.isNotBlank(product.getPrice().getCurrency())) {
        fetchedProduct.getPrice().setCurrency(product.getPrice().getCurrency());
      }

      try (ValidatorFactory validatorFactory = Validation.buildDefaultValidatorFactory()) {
        Set<ConstraintViolation<Amount>> violations = validatorFactory.getValidator().validate(fetchedProduct.getPrice());
        if (!violations.isEmpty()) {
          throw new ConstraintViolationException(violations);
        }
      }
    }
    fetchedProduct.setModified(LocalDateTime.now());
  }


}
