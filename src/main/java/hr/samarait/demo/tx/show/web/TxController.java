package hr.samarait.demo.tx.show.web;

import hr.samarait.demo.products.entity.Product;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.SortDefault;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * demo: Spring transactions
 *
 * https://www.baeldung.com/transaction-configuration-with-jpa-and-spring
 *
 * https://www.marcobehler.com/guides/spring-transaction-management-transactional-in-depth
 *
 * https://docs.spring.io/spring-framework/docs/current/reference/html/data-access.html#transaction
 *
 * https://www.baeldung.com/spring-framework-design-patterns
 */
@RestController()
@RequestMapping("/tx")
@Slf4j
public class TxController {

  private final TxService txService;

  public TxController(TxService txService) {
    this.txService = txService;
  }

  @PostMapping
  @ResponseStatus(HttpStatus.CREATED)
  public Product createProduct(@RequestBody @Valid Product product) {
    log.info("createProduct START {}", product);
    var savedProduct = txService.save(product);
    log.info("createProduct END {}", savedProduct);
    return savedProduct;
  }

  @PostMapping("/rollback")
  @ResponseStatus(HttpStatus.CREATED)
  public Product createProductRollback(@RequestBody @Valid Product product) {
    log.info("createProductRollback START {}", product);
    var savedProduct = txService.save_throw_RuntimeException_NO_annotation(product);
    log.info("createProductRollback END {}", savedProduct);
    return savedProduct;
  }

  @PostMapping("/rollback_checked")
  @ResponseStatus(HttpStatus.CREATED)
  public Product createProductRollbackCheckedException(@RequestBody @Valid Product product) throws Exception {
    log.info("createProductRollbackCheckedException START {}", product);
    var savedProduct = txService.save_throw_Exception_WITH_annotation(product);
    log.info("createProductRollbackCheckedException END {}", savedProduct);
    return savedProduct;
  }

  @PostMapping("/rollback_annotation")
  @ResponseStatus(HttpStatus.CREATED)
  public Product createProductRollbackWithAnnotation(@RequestBody @Valid Product product) {
    log.info("createProductRollbackWithAnnotation START {}", product);
    var savedProduct = txService.save_throw_RuntimeException_WITH_annotation(product);
    log.info("createProductRollbackWithAnnotation END {}", savedProduct);
    return savedProduct;
  }

  @PostMapping("/rollback_non_tx_calling_tx")
  @ResponseStatus(HttpStatus.CREATED)
  public Product createProductNonTxCallingTx(@RequestBody @Valid Product product) {
    log.info("createProductNonTxCallingTx START {}", product);
    var savedProduct = txService.saveWithoutAnnotation_callingAnnotatedMethod(product);
    log.info("createProductNonTxCallingTx END {}", savedProduct);
    return savedProduct;
  }

  @PutMapping("/{id}")
  public Product updateProduct(@PathVariable String id, @RequestBody Product product) {
    log.info("updateProduct START id={}, product={}", id, product);
    Product updatedProduct = txService.updateProduct(id, product);
    log.info("updateProduct END {}", updatedProduct);
    return updatedProduct;
  }

  @GetMapping
  public Page<Product> fetchProducts(@SortDefault(sort = "created", direction = Sort.Direction.DESC) Pageable pageable) {
    log.info("fetchProducts START {}", pageable);
    Page<Product> products = txService.findAll(pageable);
    log.info("fetchProducts END {}", products);
    return products;
  }

  @GetMapping("/{id}")
  public Product fetchProduct(@PathVariable String id) {
    log.info("fetchProduct START id={}", id);
    Product product = txService.findById(id);
    log.info("fetchProduct END {}", product);
    return product;
  }

}
