package hr.samarait.demo.config;

import io.swagger.v3.oas.models.OpenAPI;
import io.swagger.v3.oas.models.info.Info;
import org.springdoc.core.GroupedOpenApi;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class OpenApiConfig {

  @Bean
  public OpenAPI applicationDefinition() {
    return new OpenAPI()
      .info(new Info().title("Samara orders & products API")
        .description("CRUD operations on products and orders")
        .version("v0.1.0"));
  }

  @Bean
  public GroupedOpenApi ordersApi() {
    return GroupedOpenApi.builder()
      .group("orders")
      .pathsToMatch("/orders/**")
      .build();
  }

  @Bean
  public GroupedOpenApi productsApi() {
    return GroupedOpenApi.builder()
      .group("products")
      .pathsToMatch("/products/**")
      .build();
  }
}
