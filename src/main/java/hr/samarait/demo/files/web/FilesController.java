package hr.samarait.demo.files.web;

import hr.samarait.demo.common.utils.FileUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.io.InputStream;

@RestController
@RequestMapping("/files")
public class FilesController {

  @GetMapping(produces = MediaType.APPLICATION_OCTET_STREAM_VALUE)
  public byte[] fetchFile() throws IOException {
    InputStream in = getClass().getClassLoader().getResourceAsStream("images/he-man_400.jpg");
    return in.readAllBytes();
  }

  @GetMapping(value = "/image/bytes", produces = MediaType.IMAGE_JPEG_VALUE)
  public byte[] fetchImage() throws IOException {
    InputStream in = getClass().getClassLoader().getResourceAsStream("images/he-man_400.jpg");
    return in.readAllBytes();
  }

  @GetMapping(value = "/image")
  @Operation(summary = "Download image",
    responses = {@ApiResponse(responseCode = "200", content =
      {@Content(mediaType = MediaType.APPLICATION_OCTET_STREAM_VALUE), @Content(mediaType = MediaType.IMAGE_JPEG_VALUE),
        @Content(mediaType = MediaType.IMAGE_PNG_VALUE), @Content(mediaType = MediaType.IMAGE_GIF_VALUE)
      })})
  public ResponseEntity fetchImageAsStream() throws IOException {
    String fileName = "images/he-man_400.jpg";
    InputStream in = getClass().getClassLoader().getResourceAsStream(fileName);
    MediaType mediaType = FileUtils.resolveMediaType(fileName);
    return ResponseEntity.ok()
      .contentType(mediaType)
      .body(new InputStreamResource(in));
  }

}
